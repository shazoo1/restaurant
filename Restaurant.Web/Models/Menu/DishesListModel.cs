﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurant.Web.Models.Menu
{
    public class DishesListModel
    {
        public List<DishMenuModel> Dishes { get; set; }
    }
}